call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
set PATH=%PATH%;C:\Program Files\CMake\bin
mkdir build
cd build
cmake -G "NMake Makefiles" ^
  -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
  -DCMAKE_INSTALL_PREFIX=install ^
  "-DPOSTGRESQL_PG_CONFIG=C:\Program Files\Postgresql\9.6\bin\pg_config.exe" ^
  -DBOOST_LIBRARYDIR=C:\osgeo4w64\lib ^
  -DBOOST_INCLUDEDIR=C:\osgeo4w64\include\boost-1_63 ^
  -DBoost_COMPILER=-vc140 ^
  -DLIBXML2_INCLUDE_DIR=C:\osgeo4w64\include\libxml2 ^
  -DLIBXML2_LIBRARIES=C:\osgeo4w64\lib\libxml2.lib ^
  -DFCGI_INCLUDE_DIR=C:\osgeo4w64\include ^
  -DFCGI_LIBRARIES=C:\osgeo4w64\lib\libfcgi.lib ^
  -DTEMPUS_INCLUDE_DIR=c:\osgeo4w64\apps\tempus\include ^
  -DTEMPUS_LIBRARY=c:\osgeo4w64\apps\tempus\lib\tempus.lib ^
  ..
nmake && nmake install
