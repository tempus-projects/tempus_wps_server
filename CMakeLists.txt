cmake_minimum_required(VERSION 3.0)

project(tempus_wps_server VERSION 1.2.1)
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "tempus-wps-server")
set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(CPACK_PACKAGE_VENDOR        "Oslandia")
set(CPACK_PACKAGE_CONTACT       "Oslandia <infos@oslandia.com>")
set(CPACK_STRIP_FILES           TRUE)
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE")

SET(CPACK_GENERATOR "DEB")
SET(CPACK_DEBIAN_PACKAGE_DEPENDS "tempus-core (>= 2.5.0)")
SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Hugo Mercier")
include(CPack)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules")
if (WIN32)
    set (Boost_USE_STATIC_LIBS ON)
endif()
find_package(Boost REQUIRED COMPONENTS system thread)
find_package(LibXml2 REQUIRED) # our cmake module finds iconv on windows
find_package(FCGI REQUIRED)
find_package(PostgreSQL REQUIRED)

find_path(TEMPUS_INCLUDE_DIR tempus/common.hh)
find_library(TEMPUS_LIBRARY tempus)

if(MSVC)
	# Force to always compile with W4
	if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
		string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
	else()
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4" CACHE STRING "compile flags" FORCE)
	endif()
	set (CMAKE_EXE_LINKER_FLAGS "/ALLOWBIND")


	# Disable verbose warnings
	add_definitions( "/D_CRT_SECURE_NO_WARNINGS /wd4290 /wd4267 /wd4267 /wd4913" )

        # Exception model
        add_definitions( "/EHsc" )

elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)

    set (CMAKE_C_FLAGS "-fPIC")
    set (CMAKE_CXX_FLAGS "-fPIC")

    # Update if necessary
    # FIXME add Weffc++ to detect uninitialized members
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-long-long -pedantic -Wpointer-arith -Wcast-align -Wcast-qual -Woverloaded-virtual -Wformat=2 -Winit-self -Wmissing-include-dirs -Wwrite-strings -Wno-error=undef -Wshadow -Wno-error=format" CACHE STRING "compile flags" FORCE)#-Wfloat-equal -Wconversion
	# auto_ptr is deprecated in C++11
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wno-deprecated" CACHE STRING "compile flags" FORCE)
    add_definitions( -Wl,-E )
elseif(CMAKE_CXX_COMPILER MATCHES ".*clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -std=c++11 -Wno-deprecated" CACHE STRING "compile flags" FORCE)
endif()

include_directories( ${LIBXML2_INCLUDE_DIR} ${FCGI_INCLUDE_DIR} ${TEMPUS_INCLUDE_DIR} ${Boost_INCLUDE_DIR})
link_directories( ${Boost_LIBRARY_DIRS} )

add_executable( tempus_wps fastcgi.cc wps_request.cc xml_helper.cc wps_service.cc tempus_services.cc pglite/cpp/pglite.cpp )
target_link_libraries( tempus_wps ${TEMPUS_LIBRARY} ${LIBXML2_LIBRARIES} ${FCGI_LIBRARIES} ${Boost_LIBRARIES} ${PostgreSQL_LIBRARY})

if (WIN32)
  set(TEMPUS_INSTALL_DATA_DIRECTORY .)
else()
  set(TEMPUS_INSTALL_DATA_DIRECTORY "share/${CMAKE_PROJECT_NAME}")
endif()
install(DIRECTORY wps_schemas DESTINATION ${TEMPUS_INSTALL_DATA_DIRECTORY})

install( TARGETS tempus_wps DESTINATION bin )

